import javax.swing.*;
import java.io.File;
import java.io.IOException;

public class EscrituraFicheros {

    //<editor-fold desc="Variables globales">
    private static final String ficheroCliente = "clientes.txt";
    private static int codigoCliente;
    private static String nombre;
    private static long saldo;
    private static final Cliente cliente = new Cliente();
    //</editor-fold>

    //<editor-fold desc="Main">
    public static void main(String[] args) {

        menuPrincipal();

    }
    //</editor-fold>

    //<editor-fold desc="Menu principal">

    private static void menuPrincipal() {
        int opcion;

        try {
            do {
                opcion = Integer.parseInt(JOptionPane.showInputDialog(null, """
                        ****** MENU PRINCIPAL *******

                        1.-Alta cliente
                        2.-Modifica cliente
                        3.-Busqueda cliente
                        4.Salir

                        Elije una opcion"""));


                switch (opcion) {
                    case 1 -> altaCliente();
                    case 2 -> modificaCliente();
                    case 3 -> buscaCliente();
                    case 4 -> System.exit(0);
                    default -> JOptionPane.showMessageDialog(null, "Opcion incorrecta", "Error",
                            JOptionPane.ERROR_MESSAGE);
                }
            } while ((opcion < 1 || opcion > 4));
        } catch (NumberFormatException | IOException e) {
            JOptionPane.showMessageDialog(null, "Opcion incorrecta", "Error",
                    JOptionPane.ERROR_MESSAGE);
        }
        menuPrincipal();
    }
    //</editor-fold>

    //<editor-fold desc="Alta cliente">
    public static void altaCliente() throws IOException {

        //variables locales funcion alta cliente
        String datosCliente;
        String compruebaCodCliente;

        cliente.setCodigo();
        codigoCliente = cliente.getCodigo();

        //comprueba Codigo de cliente en fichero.txt
//        compruebaCodCliente = cliente.compruebaCodigoCliente(ficheroCliente,codigoCliente);

        //comprueba que no exista ningun codigo igual en el fichero
//        while (codigoCliente.equals(compruebaCodCliente) || compruebaCodCliente == null) {
//            cliente.setCodigo();
//            codigoCliente = cliente.getCodigo();
//        }
        //introducimos los datos del cliente.
        //el codigo de cleinte se introduce de forma aleatoria
        System.out.println("Codigo cliente: " + codigoCliente);
        //nombre
        cliente.setNombre();
        nombre = cliente.getNombre();
        //sueldo
        cliente.setSueldo();

        //recogemos el nombre y el sueldo
        nombre = cliente.getNombre();
        saldo = cliente.getSueldo();

        //introducimos todos los datos del cliente en una variable String
//        datosCliente = codigoCliente + "#" + nombre + "#" + saldo;

        //guardamos los datos del cliente en un fichero de texto
        cliente.guardaDatosClienteEnFicheroDeTexto(ficheroCliente,codigoCliente,nombre,saldo);


    }
    //</editor-fold>

    //<editor-fold desc="Busqueda de clientes">
    public static void buscaCliente() throws IOException {

        cliente.compruebaCliente(ficheroCliente);
        menuPrincipal();
    }
    //</editor-fold>

    //<editor-fold desc="Modificamos cliente">
    private static void modificaCliente() throws IOException {
//        String ficheroAux = "ficheroAux.txt";

//        codigoCliente = cliente.compruebaCliente(ficheroCliente);

        System.out.println("\n\tIntroduce datos a modificar\n");

        cliente.setNombre();
        nombre = cliente.getNombre();

        cliente.setSueldo();

        nombre = cliente.getNombre();
        saldo = cliente.getSueldo();

        //introducimos todos los datos del cliente en uan variable String
        String datosCliente = codigoCliente + "#" + nombre + "#" + saldo;

//        cliente.modificaDatosClienteEnFicheroTexto(ficheroCliente, ficheroAux, datosCliente, codigoCliente);

    }
    //</editor-fold>

}